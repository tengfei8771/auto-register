﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutoRegistDependency.Abstract
{
    /// <summary>
    /// 代理抽象类,动态代理需继承此类并重写他的方法。
    /// Proxy abstract class.Dynamic proxy needs to inherit this class and override its methods
    /// </summary>
    public abstract class AbstractInterceptor : IInterceptor, IAsyncInterceptor
    {
        /// <summary>
        /// 初始化Register扫描到的所有程序集。
        /// Initialize all assemblies scanned by Register class.
        /// </summary>
        protected readonly IEnumerable<Assembly> dlls;
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="dlls"></param>
        public AbstractInterceptor(IEnumerable<Assembly> dlls)
        {
            this.dlls = dlls;
        }
        /// <summary>
        /// 此方法获取此拦截器所有需要代理的类。
        /// This method obtains all classes that require proxies for this interceptor.
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<Type> GetInterceptorTypes();
        /// <summary>
        /// 将同步代理转为异步代理。如有特殊需求可以重写此方法。
        /// Convert synchronous proxy to asynchronous proxy.Override if needed.
        /// </summary>
        /// <param name="invocation">代理方法的调用。Encapsulates an invocation of a proxied method.</param>
        public virtual void Intercept(IInvocation invocation)
        {
            this.ToInterceptor().Intercept(invocation);
        }
        /// <summary>
        /// 返回值为Task的异步方法都会走这个方法。
        /// Asynchronous methods with a return value of Task will use this method
        /// </summary>
        /// <param name="invocation">代理方法的调用。Encapsulates an invocation of a proxied method.</param>
        public abstract void InterceptAsynchronous(IInvocation invocation);
        /// <summary>
        /// 返回值为泛型Task的异步方法都会走这个方法。
        /// Asynchronous methods that return a generic Task will use this method.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="invocation">代理方法的调用。Encapsulates an invocation of a proxied method.</param>
        public abstract void InterceptAsynchronous<TResult>(IInvocation invocation);
        /// <summary>
        /// 同步方法都会走这里。
        /// All synchronization methods will will use this method.
        /// </summary>
        /// <param name="invocation">代理方法的调用。Encapsulates an invocation of a proxied method.</param>
        public abstract void InterceptSynchronous(IInvocation invocation);
    }
}
