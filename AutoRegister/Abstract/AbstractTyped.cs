﻿using Autofac;
using Autofac.Core;
using Autofac.Features.AttributeFilters;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Utils;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AutoRegistDependency.Abstract
{
    /// <summary>
    /// 抽象类 只包含组件name和key
    /// </summary>
    public abstract class AbstractTyped: ParameterFilterAttribute
    {
        /// <summary>
        /// 服务名称
        /// </summary>
        public string Name { get; protected set; }
        /// <summary>
        /// 服务key
        /// </summary>
        public object Key { get; protected set; }

        public override bool CanResolveParameter(ParameterInfo parameter, IComponentContext context)
        {
            var service = AutoFacHelper.GetResolveServiceKey(Name, Key, parameter.ParameterType);
            return context.IsRegisteredService(service);
            
        }
        public override object ResolveParameter(ParameterInfo parameter, IComponentContext context)
        {
            var service = AutoFacHelper.GetResolveServiceKey(Name, Key, parameter.ParameterType);
            return context.ResolveService(service);
        }
    }
}
