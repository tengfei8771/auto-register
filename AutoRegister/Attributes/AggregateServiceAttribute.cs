﻿using AutoRegistDependency.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 聚合服务特性。只能标记在接口上,不需要实现类。
    /// 聚合服务不会被任何其他的代理类代理，同时也不受到IBeanPostProcessor的增强。
    /// 聚合服务定义接口一定要是同步方法,异步方法暂不支持。
    /// Aggregation service characteristics. It can only be marked on an interface and does not require an implementation class.
    /// The aggregation service will not be proxied by any other proxy class, nor will it be enhanced by IBeanPostProcessor.
    /// The aggregation service definition interface must be a synchronous method, and asynchronous methods are currently not supported.
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public sealed class AggregateServiceAttribute : Attribute
    {
        /// <summary>
        /// 生命周期
        /// </summary>
        public LifeTimeType Lifetime { get; private set; }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="lifetime">生命周期</param>
        public AggregateServiceAttribute(LifeTimeType lifetime = LifeTimeType.Default)
        {
            Lifetime = lifetime;
        }
    }
}
