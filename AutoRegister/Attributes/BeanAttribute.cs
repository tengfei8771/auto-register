﻿using AutoRegistDependency.Enum;
using System;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 方法上进行标记。将返回的实例注册到容器中(返回值为Task或者void的方法不会注册)。
    /// Bean所在的类上无ConfigurationAttribute或者ComponentAttribute特性将不会注册。
    /// 如果注册的方法是个泛型方法,那么泛型方法的返回值也必须是个泛型，且返回值的泛型的数量和顺序必须和方法一致。
    /// 例如:public BeanTest1 &lt;T&gt; TestBean1 &lt;T&gt;() where T: class=>new BeanTest1 &lt;T&gt;();BeanTest1 为泛型类，在注册时会当成开放泛型进行注册。否则泛型方法返回的实例不会被注册。
    /// Mark the method. Register the returned instance into the container (methods with a return value of Task or void will not be registered). 
	/// If there is no ConfigurationAttribute or ComponentAttribute on the class where the bean is located, it will not be registered.
    /// If the registered method is a generic method, the return value of the generic method must also be a generic, and the number and order of generics for the return value must be consistent with the method.
    /// For example: public BeanTest1 &lt;T&gt; TestBean1 &lt;T&gt;() where T: class=>new BeanTest1 &lt;T&gt;(); BeanTest1 is a generic class that will be registered as an open generic during registration. 
    /// Otherwise, the instance returned by the generic method will not be registered.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public sealed class BeanAttribute : Attribute
    {
        /// <summary>
        /// 解析的服务name
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 解析的服务key
        /// </summary>
        public object Key { get; }
        /// <summary>
        /// 生命周期
        /// </summary>
        public LifeTimeType LifeTimeType { get; set; }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="name">服务名</param>
        /// <param name="lifeTimeType">生命周期</param>
        public BeanAttribute(string name = "", LifeTimeType lifeTimeType = LifeTimeType.Default)
        {
            Name = name;
            LifeTimeType = lifeTimeType;
        }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="key">服务key</param>
        /// <param name="lifeTimeType">生命周期</param>
        public BeanAttribute(object key, LifeTimeType lifeTimeType = LifeTimeType.Default)
        {
            Key = key;
            LifeTimeType = lifeTimeType;
        }
    }
}
