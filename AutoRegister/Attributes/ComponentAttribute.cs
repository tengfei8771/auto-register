﻿using AutoRegistDependency.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 组件注入特性。可以标记在类上。
    /// Component、Confignature、Job以及ConfigurationProperties特性在同一个类上只能有一个。
    /// Component injection attribute. Can be marked on a class. 
	/// The Component, Configure,Job,ConfigurationProperties attributes can only have one on the same class.
    /// </summary>
    [Unique]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class ComponentAttribute : Attribute
    {
        /// <summary>
        /// 生命周期
        /// </summary>
        public LifeTimeType Lifetime { get; set; }
        /// <summary>
        /// 是否自动激活实例
        /// </summary>
        public bool AutoActivate { get; }
        /// <summary>
        /// 服务name
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 属性注入类型
        /// </summary>
        public PropertyInjectType PropertyInjectType { get; }
        /// <summary>
        /// 注册方式
        /// </summary>
        public RegistType RegistType { get; set; }
        /// <summary>
        /// 服务key
        /// </summary>
        public object Key { get; }
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="lifetime">生命周期</param>
        /// <param name="autoActivate">是否自动激活实例</param>
        /// <param name="name">服务名(默认类名简称)</param>
        /// <param name="key">服务key</param>
        /// <param name="registType">注册类型(注册自己本身还是作为接口实现类)</param>
        /// <param name="propertyInjectType">autofac属性注入方式,是否通过特性标记需要注入的属性</param>
        public ComponentAttribute(LifeTimeType lifetime = LifeTimeType.Default, bool autoActivate = false,
            string name = "", object key = null,
            RegistType registType = RegistType.Default,
            PropertyInjectType propertyInjectType = PropertyInjectType.Default)
        {
            Lifetime = lifetime;
            AutoActivate = autoActivate;
            Name = name;
            PropertyInjectType = propertyInjectType;
            Key = key;
            RegistType = registType;
        }

    }
}
