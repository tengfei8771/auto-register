﻿using Autofac;
using Autofac.Features.AttributeFilters;
using AutoRegistDependency.Abstract;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 组件筛选特性。标记在构造函数的参数上,可以自动获取目标实现类。
    /// Component filtering attribute. 
	/// Mark on the parameters of the constructor to automatically obtain the target implementation class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public sealed class ComponentFilterAttribute : AbstractTyped
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="name">服务name</param>
        public ComponentFilterAttribute(string name = "")
        {
            Name = name;
        }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="key">服务key</param>
        public ComponentFilterAttribute(object key)
        {
            Key = key;
        }
    }
}
