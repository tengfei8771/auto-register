﻿using Autofac.Core.Registration;
using AutoRegistDependency.Component;
using AutoRegistDependency.Interface;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes.Condition
{
    /// <summary>
    /// 根据配置文件项决定组件是否注册特性。可以标记在方法以及类上。
    /// Determine whether the component registers features based on the configuration file items. Can be marked on methods and classes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public sealed class ConditionalOnPropertyAttribute : Attribute, ICondition
    {
        /// <summary>
        /// 配置路径前缀
        /// </summary>
        public string Prefix { get; private set; }
        /// <summary>
        /// 配置项name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 配置项的值
        /// </summary>
        public string HavingValue { get; private set; }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="name">配置文件内属性的名称。The name of the attribute in the configuration file.</param>
        /// <param name="havingValue">配置文件值。Profile values</param>
        /// <param name="prefix">前缀</param>
        public ConditionalOnPropertyAttribute(string name, string havingValue, string prefix = "")
        {
            Prefix = prefix;
            Name = name;
            HavingValue = havingValue;
        }

        public bool Matches(HostBuilderContext hostBuilderContext, IComponentRegistryBuilder componentRegistry, ComponentDefinition component)
        {
            if (hostBuilderContext == null)
            {
                return true;
            }
            else
            {
                string totalPath = !string.IsNullOrWhiteSpace(Prefix) ? $"{Prefix}:{Name}" : Name;
                return hostBuilderContext.Configuration.GetSection(totalPath).Value == HavingValue;
            }
        }
    }
}
