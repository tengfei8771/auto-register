﻿using AutoRegistDependency.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 条件注册特性。可以标记在类以及方法上。根据不同的条件实现类判断是否注册标记的服务。
    /// Conditional registration attribute. Can be marked on classes and methods. 
    /// Implement classes based on different conditions to determine whether to register marked services.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public sealed class ConditionalAttribute : Attribute
    {
        /// <summary>
        /// ICondition类
        /// </summary>
        public Type[] ConditionTypes { get; private set; }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="conditionTypes">必须为实现了ICondition接口的类。Must be a class that implements the ICondition interface</param>
        public ConditionalAttribute(params Type[] conditionTypes)
        {
            ConditionTypes = conditionTypes;
        }
    }
}
