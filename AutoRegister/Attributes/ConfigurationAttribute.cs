﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 标记此特性代表容器管理此类型的一个单例,通常作为配置类管理需要注册的服务需要和BeanAttribte配合使用。
    /// Component、Confignature以及Job特性在同一个类上只能有一个。
    /// Marking this Attribute represents a singleton of container management for this type, typically used in conjunction with BeanAttribute as a configuration class to manage registered services. 
    /// The Component, Configure, and Job attributes can only have one on the same class.
    /// </summary>
    [Unique]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class ConfigurationAttribute:Attribute
	{
	}
}
