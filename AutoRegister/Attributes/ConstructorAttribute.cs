﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
	/// <summary>
	/// Autofac注入时默认使用参数最多的构造函数,使用此特性指定所使用的构造函数。
	/// 请注意：如果使用此特性指定构造函数,即便在拦截器内的GetInterceptorTypes()方法内声明代理此类,此类的类代理也不会生效。
	/// 无参构造函数上标记此特性无效。
	/// 此特性在同一个类中只能有一个。
	/// 非构造函数指明此特性无效。
	/// By default, the constructor with the most parameters is used during Autofac injection, and this attribute is used to specify the constructor to be used.
	/// Please note that if this attribute is used to specify a constructor, even if a proxy class is declared within the GetInterceptorTypes() method within the interceptor, the class proxy for this class will not take effect.
	/// This attribute is not valid when marked on an unarmed constructor.
	/// This attribute can only have one in the same class.
	/// Non constructor indicates that this attribute is invalid.
	/// </summary>
	[AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false, Inherited = false)]
	public sealed class ConstructorAttribute : Attribute
	{
	}
}
