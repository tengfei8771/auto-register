﻿using Autofac.Core;
using AutoRegistDependency.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 组件依赖特性
    /// 使用此组件可标记当前组件初始化之前需要的其他组件并初始化并缓存。
    /// 同时使用构造函数注入组件时，会优先使用和初始化类型相同的缓存类型。如果构造函数参数标记了[ComponentFilterAttribute]特性，不会使用此参数的缓存实例。
    /// 如果此特性标记在[Bean]方法上，将会对Bean方法的入参实例进行优先使用。
    /// Component dependency characteristics.
    /// Use this component to mark other components required before initializing the current component and initialize and cache them. When using constructors to inject components simultaneously, cache types with the same initialization type will be prioritized.
    /// If the constructor parameter is marked with the [ComponentFilterAttribute], the cached instance of this parameter will not be used.
    /// If this feature is marked on the [Bean] method, it will prioritize the use of the bean method's input instance.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public sealed class DependsOnAttribute : AbstractTyped
    {
        public Type CompnentType { get; set; }
        public DependsOnAttribute(Type compnentType,string name="")
        {
            CompnentType = compnentType;
            if (string.IsNullOrWhiteSpace(name))
            {
                Name = CompnentType.Name;
            }
        }
        public DependsOnAttribute(Type compnentType,object key)
        {
            Key = key;
            CompnentType = compnentType;
        }
    }
}
