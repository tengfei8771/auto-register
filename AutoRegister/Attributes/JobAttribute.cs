﻿using Autofac.Core.Lifetime;
using AutoRegistDependency.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 定时任务注册特性。仅能标记在类上。
    /// Component、Confignature、Job以及ConfigurationProperties特性在同一个类上只能有一个。
    /// 注册之后还需调用RegisterExtension.QuartzRegist方法才能成功启动任务。
    /// 如果定时任务类需要使用动态代理,一定要将Execute方法声明为virtual！因为定时任务启动时调用的是IJob的实现类而非IJob接口！
    /// Timer task registration feature. Can only be marked on a class.
    /// The Component, Configure, Job, and ConfigurationProperties attributes can only have one on the same class.
    /// After registration, the RegisterExtension.QuartzRegister method needs to be called to successfully start the task.
    /// If the scheduled task class requires the use of dynamic proxies, be sure to declare the Execute method as virtual! 
    /// Because the implementation class of IJob is called when the scheduled task starts, not the IJob interface!
    /// </summary>
    [Unique]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class JobAttribute : Attribute
    {
        /// <summary>
        /// cron表达式
        /// </summary>
        public string Cron { get; private set; }
        /// <summary>
        /// 优先级
        /// </summary>
        public int Priority { get; private set; }
        /// <summary>
        /// 生命周期
        /// </summary>
        public LifeTimeType LifeTime { get; set; }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="cron">cron表达式</param>
        /// <param name="priority">执行优先级 0-5</param>
        /// <param name="lifeTime">生命周期</param>
        public JobAttribute(string cron, int priority = 0, LifeTimeType lifeTime = LifeTimeType.Default)
        {
            Cron = cron;
            Priority = priority;
            LifeTime = lifeTime;
        }
    }
}
