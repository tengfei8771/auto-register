﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 组件注册顺序特性。值越小越先注册(对bean无效)
    /// Component registration order feature. Register first with smaller values (invalid for bean)
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class OrderAttribute : Attribute
    {
        /// <summary>
        /// 排序
        /// </summary>
        public int Order { get; } = 0;
        /// <summary>
        /// 组件注册排序
        /// </summary>
        /// <param name="order">排序</param>
        public OrderAttribute(int order)
        {
            this.Order = order;
        }

    }
}
