﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes.Processor
{
    /// <summary>
    /// Bean增强特性。可指定组件在实例激活完成之后(所有的依赖注入工作完成后)立即执行组件内的某个方法。
    /// Bean enhanced features. You can specify that a component immediately executes a method within the component after the instance activation is completed (after all dependency injection work is completed).
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public sealed class OnActivatedAttribute : Attribute
    {

    }
}
