﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes.Processor
{
    /// <summary>
    /// bean增强特性。可指定组件在实例激活时(指仅执行了构造函数)，立即执行组件内的某个方法。
    /// Bean enhancement features. You can specify that a component immediately executes a method within the component when the instance is activated (referring to only executing the constructor).
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class OnActivatingAttribute : Attribute
    {

    }
}
