﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes.Processor
{
    /// <summary>
    /// bean增强特性。可指定某个组件在容器销毁之前执行某个方法。需要注意的是,组件销毁时依赖注入上下文已经销毁，所以此增强方式仅支持无参方法。
    /// Bean enhancement features. You can specify a component to execute a method before the container is destroyed. It should be noted that the dependency injection context has already been destroyed when the component is destroyed, so this enhancement only supports parameter free methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public sealed class OnReleaseAttribute : Attribute
    {
    }
}
