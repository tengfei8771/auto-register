﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
    /// <summary>
    /// 注册目标类型特性，可标记在类上和方法上。目标类型必须为此类实现的接口或父类。
    /// Register target type attributes that can be marked on classes and methods.
    /// The target type must be an interface or parent class implemented by this class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class RegistAsAttribute : Attribute
    {
        /// <summary>
        /// 注册目标类型
        /// </summary>
        public Type[] Types { get; set; }
        /// <summary>
        /// 注册目标类型
        /// </summary>
        /// <param name="types">类型</param>
        public RegistAsAttribute(params Type[] types)
        {
            Types = types;
        }
        /// <summary>
        /// 注册目标类型
        /// </summary>
        /// <param name="typeNames">类型名(全称)</param>
        public RegistAsAttribute(params string[] typeNames)
        {
            var types = new Type[typeNames.Length];
            for (int i = 0; i < typeNames.Length; i++)
            {
                types[i] = Type.GetType(typeNames[i]);
            }
            this.Types = types;
        }
    }
}
