﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Attributes
{
	/// <summary>
	/// 无业务用处 标记此特性代表不可以与其他带有此特性的特性同时标记
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	internal class UniqueAttribute:Attribute
	{

	}
}
