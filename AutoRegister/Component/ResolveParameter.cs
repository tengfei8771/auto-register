﻿using Autofac;
using Autofac.Core;
using Autofac.Core.Resolving;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AutoRegistDependency.Component
{
    internal class ResolveParameter : Parameter
    {
        private readonly ConcurrentDictionary<Service, object> serviceCache = new ConcurrentDictionary<Service, object>();
        public void AddService(Service service, object instance)
        {
            serviceCache.TryAdd(service, instance);
        }
        public bool TryGetCache(Service service, out object instance)
        {
            return serviceCache.TryGetValue(service, out instance);
        }
        public void RemoveFirstKey()
        {
            serviceCache.TryRemove(serviceCache.Keys.FirstOrDefault(), out object instance);
        }

        public override bool CanSupplyValue(ParameterInfo pi, IComponentContext context, out Func<object> valueProvider)
        {
            valueProvider = null;
            return false;
        }
    }
}
