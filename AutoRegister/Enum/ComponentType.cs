﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Enum
{
    internal enum ComponentType
    {
        //服务组件
        Component,
        //被管理的类
        Configuration,
        //配置文件模型类
        ConfigurationProperties,
        //bean类型为method的返回类型 在Configuration类下有效
        Bean,
        //拦截器类
        Interceptor,
        //定时任务类
        Job,
        //bean增强器
        BeanPostProcessor,
        //聚合服务
        AggregateService,
    }
}
