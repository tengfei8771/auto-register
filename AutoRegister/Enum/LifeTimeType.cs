﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Enum
{
	/// <summary>
	/// 声明周期
	/// </summary>
	public enum LifeTimeType
	{
		/// <summary>
		/// 默认
		/// </summary>
		Default,
		/// <summary>
		/// scope
		/// </summary>
		InstancePerLifetimeScope,
		/// <summary>
		/// 单例
		/// </summary>
		SingleInstance,
		/// <summary>
		/// 瞬时
		/// </summary>
		InstancePerDependency,
		/// <summary>
		/// 每次请求
		/// </summary>
		InstancePerRequest
	}
}
