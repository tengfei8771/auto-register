﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Enum
{
	/// <summary>
	/// 属性注入类型(字段注入必须要有autorwied特性)
	/// </summary>
	public enum PropertyInjectType
	{
		/// <summary>
		/// 默认
		/// </summary>
		Default,
		/// <summary>
		/// 标记autowired
		/// </summary>
		Autowired,
		/// <summary>
		/// 所有
		/// </summary>
		ALL
	}
}
