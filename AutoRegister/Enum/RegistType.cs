﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Enum
{
	/// <summary>
	/// 注册类型
	/// </summary>
	public enum RegistType
	{
		/// <summary>
		/// 默认
		/// </summary>
		Default,
		/// <summary>
		/// 注册实现的接口和自身
		/// </summary>
		InterfaceAndImplement,
		/// <summary>
		/// 仅注册自身实现的接口
		/// </summary>
		Interface,
		/// <summary>
		/// 仅注册自身
		/// </summary>
		Self
	}
}
