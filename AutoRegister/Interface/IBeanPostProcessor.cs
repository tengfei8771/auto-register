﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// bean增强器。可对组件初始化前后对组件功能进行增强。
    /// Bean enhancer. It is possible to enhance the functionality of components before and after initialization.
    /// </summary>
    public interface IBeanPostProcessor
	{
        /// <summary>
        /// 构造函数构造对象成功之后对bean进行增强。
        /// Enhance the bean after the constructor successfully constructs the object
        /// </summary>
        /// <param name="bean">bean实例</param>
        /// <returns>增强后的bean实例</returns>
        object PostProcessBeforeInitialization(object bean);
        /// <summary>
        /// 字段、方法、属性注入等成功之后调用此方法。
        /// This method is called after successful injection of fields, methods, and attributes.
        /// </summary>
        /// <param name="bean">bean实例</param>
        /// <returns>增强后的bean实例</returns>
        object PostProcessAfterInitialization(object bean);
	}
}
