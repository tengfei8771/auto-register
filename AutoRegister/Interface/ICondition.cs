﻿using Autofac.Core.Registration;
using AutoRegistDependency.Component;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 注册条件判断接口。
    /// Registration condition judgment interface
    /// </summary>
    public interface ICondition
	{
        /// <summary>
        /// 匹配注册条件
        /// </summary>
        /// <param name="hostBuilderContext">host上下文</param>
        /// <param name="componentRegistry">注册上下文</param>
        /// <param name="component">组件注册信息</param>
        /// <returns>true注册 false不注册</returns>
        bool Matches(HostBuilderContext hostBuilderContext, IComponentRegistryBuilder componentRegistry, ComponentDefinition component);
	}
}
