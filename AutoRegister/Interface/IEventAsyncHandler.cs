﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 异步消费接口(无返回值)
    /// </summary>
    /// <typeparam name="T">发布类型</typeparam>
    public interface IEventAsyncHandler<T>
    {
        /// <summary>
        /// 消费
        /// </summary>
        /// <param name="eventData">订阅数据</param>
        Task Handle(T eventData);
    }
    /// <summary>
    /// 异步消费接口（有返回值）
    /// </summary>
    /// <typeparam name="T">发布类型</typeparam>
    /// <typeparam name="TResult">消费结果类型</typeparam>
    public interface IEventAsyncHandler<T, TResult>
    {
        /// <summary>
        /// 消费
        /// </summary>
        /// <param name="eventData">订阅数据</param>
        Task<TResult> Handle(T eventData);
    }
}
