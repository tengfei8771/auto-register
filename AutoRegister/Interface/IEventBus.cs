﻿using Quartz.Impl.AdoJobStore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 事件总线接口
    /// </summary>
    public interface IEventBus
    {
        /// <summary>
        /// 同步发布消息
        /// </summary>
        /// <typeparam name="T">eventData类型</typeparam>
        /// <param name="eventData">消息数据</param>
        void Publish<T>(T eventData);
        /// <summary>
        /// 异步发布消息
        /// </summary>
        /// <typeparam name="T">eventData类型</typeparam>
        /// <param name="eventData">消息数据</param>
        /// <returns></returns>
        Task PublishAsync<T>(T eventData);
        /// <summary>
        /// 发布消息(有返回值)
        /// </summary>
        /// <typeparam name="T">eventData类型</typeparam>
        /// <typeparam name="TResult">返回结果类</typeparam>
        /// <param name="eventData">消息数据</param>
        /// <returns></returns>
        List<TResult> Publish<T, TResult>(T eventData);
        /// <summary>
        /// 异步发布消息(有返回值)
        /// </summary>
        /// <typeparam name="T">eventData类型</typeparam>
        /// <typeparam name="TResult">返回结果类</typeparam>
        /// <param name="eventData">消息数据</param>
        /// <returns></returns>
        Task<List<TResult>> PublishAsync<T, TResult>(T eventData);
    }
}
