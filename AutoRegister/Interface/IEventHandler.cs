﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 订阅者处理接口
    /// </summary>
    public interface IEventHandler<T>
    {
        /// <summary>
        /// 消费
        /// </summary>
        /// <param name="eventData">订阅数据</param>
        void Handle(T eventData);
    }
    /// <summary>
    /// 订阅者处理接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface IEventHandler<T, TResult>
    {
        /// <summary>
        /// 消费
        /// </summary>
        /// <param name="eventData"></param>
        /// <returns></returns>
        TResult Handle(T eventData);
    }
}
