﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 使用value获取配置文件值后对value继续进行处理的方法
    /// </summary>
    public interface IValueHandler
    {
        /// <summary>
        /// 执行转换方法
        /// </summary>
        /// <param name="value">通过配置文件获取的值</param>
        /// <returns></returns>
        object Execute(object value);
    }
}
