﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.Interface
{
    /// <summary>
    /// 使用value获取配置文件值后对value继续进行处理的方法
    /// The method of using value to obtain the configuration file value and then continuing to process the value
    /// </summary>
    public interface IValuePostHandler
    {
        /// <summary>
        /// 执行转换方法
        /// Execute conversion method
        /// </summary>
        /// <param name="value">通过配置文件获取的值 Value obtained through configuration file</param>
        /// <returns></returns>
        object Execute(object value);
    }
}
