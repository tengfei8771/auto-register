﻿using Autofac;
using Autofac.Core.Registration;
using Autofac.Features.Variance;
using AutoRegistDependency.Abstract;
using AutoRegistDependency.Attributes;
using AutoRegistDependency.Component;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Implement;
using AutoRegistDependency.Interface;
using AutoRegistDependency.Module;
using AutoRegistDependency.RegistrationHandler;
using AutoRegistDependency.Utils;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Module = Autofac.Module;

namespace AutoRegister
{
    /// <summary>
    /// 注册类 所有的注册逻辑都在此类实现
    /// </summary>
    public class Register : Module
    {
        private List<Assembly> assemblies = new List<Assembly>();
        private LifeTimeType defaultLifeTime = LifeTimeType.InstancePerLifetimeScope;
        private PropertyInjectType defaultPropertyInjectType = PropertyInjectType.Autowired;
        private RegistType defaultRegistType = RegistType.InterfaceAndImplement;
        private bool enableEventBus = true;
        private Type defaultEventBusImpl = typeof(EventBus);
        /// <summary>
        /// 注册信息表
        /// </summary>
        public ConcurrentDictionary<Type, List<ComponentDefinition>> TypeMapInfo { get; private set; } = new ConcurrentDictionary<Type, List<ComponentDefinition>>();
        private List<AbstractInterceptor> interceptors = new List<AbstractInterceptor>();
        private HostBuilderContext hostBuilderContext;
        private ConcurrentDictionary<Type, ICondition> conditionMap = new ConcurrentDictionary<Type, ICondition>();
        private bool hasBeanPostProcessor = false;
        private bool enableQuartz = true;
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="hostBuilderContext">host上下文</param>
        public Register(HostBuilderContext hostBuilderContext)
        {
            this.hostBuilderContext = hostBuilderContext;
            assemblies = AutoFacHelper.GetAssemblies();
        }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="hostBuilderContext">host上下文</param>
        /// <param name="assemblies">指定扫描程序集</param>
        public Register(HostBuilderContext hostBuilderContext, params Assembly[] assemblies)
        {
            this.hostBuilderContext = hostBuilderContext;
            this.assemblies = assemblies.ToList();
        }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="hostBuilderContext">host上下文</param>
        /// <param name="assemblyNames">指定程序集字符串名称</param>
        public Register(HostBuilderContext hostBuilderContext, params string[] assemblyNames)
        {
            this.hostBuilderContext = hostBuilderContext;
            foreach (string name in assemblyNames)
            {
                Assembly assembly = Assembly.LoadFrom(name);
                if (assembly != null)
                {
                    assemblies.Add(assembly);
                }
            }
        }
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="hostBuilderContext">host上下文</param>
        /// <param name="types">被扫描的程序集所有的类</param>
        public Register(HostBuilderContext hostBuilderContext, params Type[] types)
        {
            this.hostBuilderContext = hostBuilderContext;
            foreach (Type type in types)
            {
                Assembly assembly = Assembly.GetAssembly(type);
                if (assembly != null)
                {
                    assemblies.Add(assembly);
                }
            }
        }
        /// <summary>
        /// 设置默认生命周期。
        /// Set default lifetime
        /// </summary>
        /// <param name="lifetime">生命周期</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Register SetDefaultLifeTime(LifeTimeType lifetime)
        {
            if (lifetime == LifeTimeType.Default)
            {
                throw new ArgumentException("global lifeTime can not be Default!");
            }
            this.defaultLifeTime = lifetime;
            return this;
        }
        /// <summary>
        /// 设置属性注入类型
        /// </summary>
        /// <param name="propertyInjectType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Register SetDefaultPropertyInjectType(PropertyInjectType propertyInjectType)
        {
            if (propertyInjectType == PropertyInjectType.Default)
            {
                throw new ArgumentException("global PropertyInjectType can not be Default!");
            }
            this.defaultPropertyInjectType = propertyInjectType;
            return this;
        }
        /// <summary>
        /// 设置默认注册类型
        /// </summary>
        /// <param name="registType">注册类型</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Register SetDefaultRegistType(RegistType registType)
        {
            if (registType == RegistType.Default)
            {
                throw new ArgumentException("global RegistType can not be Default!");
            }
            this.defaultRegistType = registType;
            return this;
        }
        /// <summary>
        /// 设置是否启用事件总线。
        /// Set whether to enable event bus
        /// </summary>
        /// <param name="enableEventBus">是否启用事件总线</param>
        /// <returns></returns>
        public Register SetEnabeleEventBus(bool enableEventBus)
        {
            this.enableEventBus = enableEventBus;
            return this;
        }
        /// <summary>
        /// 设置事件总线的类型
        /// </summary>
        /// <param name="eventBusType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public Register SetEventBusType(Type eventBusType)
        {
            if (!eventBusType.IsClass || typeof(IEventBus).IsAssignableFrom(eventBusType))
            {
                throw new ArgumentException($"eventBusType must be Implement IEventBus!!");
            }
            else
            {
                this.defaultEventBusImpl = eventBusType;
            }
            return this;
        }
        /// <summary>
        /// 设置是否使用quartz。
        /// Set whether to use quartz.
        /// </summary>
        /// <param name="enableQuartz"></param>
        /// <returns></returns>
        public Register SetEnableQuartz(bool enableQuartz)
        {
            this.enableQuartz = enableQuartz;
            return this;
        }
        protected override void Load(ContainerBuilder builder)
        {
            RegisterType(builder);
        }
        /// <summary>
        /// 注册所有带有特性的类
        /// </summary>
        /// <param name="containerBuilder">容器</param>
        public void RegisterType(ContainerBuilder containerBuilder)
        {
            if (enableEventBus)
            {
                containerBuilder.RegisterSource(new ContravariantRegistrationSource());
                containerBuilder.RegisterType(defaultEventBusImpl).AsSelf().AsImplementedInterfaces().SingleInstance();
            }
            if (enableQuartz)
            {
                containerBuilder.RegisterModule(new QuartzAutofacFactoryModule());
            }
            InitRegistType(containerBuilder.ComponentRegistryBuilder);
            List<ComponentDefinition> components = TypeMapInfo.Values.SelectMany(t => t).ToList();
            hasBeanPostProcessor = components.Any(t => t.ComponentType == ComponentType.BeanPostProcessor);
            containerBuilder.RegisterInstance(this).SingleInstance();
            var componentHandler = CreateCompleteHandler();
            foreach (var component in components
                .OrderBy(t => t.Conditions.Count())
                .ThenBy(t => t.Order)
                .ThenBy(t => t.Name))
            {
                foreach (IGrouping<bool, Type> group in component.RegistAs.GroupBy(t => t.IsInterface))
                {
                    var r= componentHandler.StartRegistration(containerBuilder, component, group.Key, group.ToArray(), hasBeanPostProcessor, hostBuilderContext);
                }

            }
        }
        /// <summary>
        /// 构造一个完整的责任链handler
        /// </summary>
        /// <returns></returns>
        private AbstractRegistrationHandler CreateCompleteHandler()
        {
            ComponentHandler componentHandler = new ComponentHandler();
            AggregateServiceHandler aggregateServiceHandler = new AggregateServiceHandler();
            BeanHandler beanHandler = new BeanHandler();
            GenericComponentHandler genericComponentHandler = new GenericComponentHandler();
            GenericAggregateServiceHandler genericAggregateServiceHandler = new GenericAggregateServiceHandler();
            GenericBeanHandler genericBeanHandler = new GenericBeanHandler();
            componentHandler.Next(genericComponentHandler);
            genericComponentHandler.Next(aggregateServiceHandler);
            aggregateServiceHandler.Next(genericAggregateServiceHandler);
            genericAggregateServiceHandler.Next(beanHandler);
            beanHandler.Next(genericBeanHandler);
            return componentHandler;
        }

        /// <summary>
        /// 初始化注册类型信息
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        private void InitRegistType(IComponentRegistryBuilder componentRegistry)
        {
            if (assemblies.Count() == 0)
            {
                throw new ArgumentNullException("can not load any assembly!");
            }
            var types = assemblies.SelectMany(t => t.GetTypes())
                .Where(t => !t.IsAbstract && t.IsClass &&
                t.GetCustomAttributes().Any(a => a.GetType() == typeof(ComponentAttribute)
                || a.GetType() == typeof(ConfigurationPropertiesAttribute)
                || a.GetType() == typeof(ConfigurationAttribute)
                || a.GetType() == typeof(JobAttribute)))
                .Union(
                assemblies.SelectMany(t => t.GetTypes())
                .Where(t => t.IsInterface && t.GetCustomAttributes()
                .Any(a => a.GetType() == typeof(AggregateServiceAttribute)))
                )
                .OrderBy(t =>
                {
                    var attr = t.GetCustomAttribute<OrderAttribute>();
                    if (attr != null)
                    {
                        return attr.Order;
                    }
                    else
                    {
                        return 999;
                    }
                })
                .ThenBy(t => t.Name)
                .ToList();
            foreach (Type type in types)
            {
                var componentAttribute = type.GetCustomAttribute<ComponentAttribute>();
                var confignatrueAttribute = type.GetCustomAttribute<ConfigurationPropertiesAttribute>();
                var confignation = type.GetCustomAttribute<ConfigurationAttribute>();
                //服务扫描
                var registInfos = new ComponentDefinition(type, defaultLifeTime, defaultPropertyInjectType, defaultRegistType)
                    .GetComponentTypeRegistInfos();
                foreach (ComponentDefinition registInfo in registInfos)
                {
                    if (type.IsSubclassOf(typeof(AbstractInterceptor)))
                    {
                        var constructor = type.GetConstructors()
                                    .Where(t => t.GetParameters().Select(a => a.ParameterType).Contains(typeof(IEnumerable<Assembly>)))
                                    .FirstOrDefault();
                        if (constructor != null)
                        {
                            var constructorParameters = constructor.GetParameters();
                            object[] invokeParameters = new object[constructorParameters.Length];
                            for (int i = 0; i < constructor.GetParameters().Length; i++)
                            {
                                if (constructorParameters[i].ParameterType == typeof(IEnumerable<Assembly>))
                                {
                                    invokeParameters[i] = this.assemblies;
                                }
                                else
                                {
                                    invokeParameters[i] = AutoFacHelper.GetDefaultValue(constructorParameters[i].ParameterType);
                                }
                            }
                            AbstractInterceptor interceptor = (AbstractInterceptor)constructor.Invoke(invokeParameters);
                            interceptors.Add(interceptor);
                        }
                    }
                    if (TypeMapInfo.TryGetValue(registInfo.InjectType, out var componentTypeRegistInfos))
                    {
                        componentTypeRegistInfos.Add(registInfo);
                    }
                    else
                    {
                        TypeMapInfo.TryAdd(registInfo.InjectType, new List<ComponentDefinition>() { registInfo });
                    }
                }
            }
            AppendInterceporInfo();
        }
        /// <summary>
        /// 对注册信息附加拦截器信息
        /// </summary>
        private void AppendInterceporInfo()
        {
            foreach (AbstractInterceptor interceptor in interceptors)
            {
                var interceptorTypes = interceptor.GetInterceptorTypes();
                foreach (var interceptorType in interceptorTypes)
                {
                    if (TypeMapInfo.TryGetValue(interceptorType, out var result))
                    {
                        result.ForEach(t =>
                        {
                            t.Interceptors.Add(interceptor);
                        });
                    }
                }
            }
        }

    }
    //public class AutowiredSelector : IPropertySelector
    //{
    //    public bool InjectProperty(PropertyInfo propertyInfo, object instance)
    //    {
    //        return propertyInfo.CustomAttributes.Any(t => t.AttributeType == typeof(AutowiredAttribute));
    //    }
    //}
}
