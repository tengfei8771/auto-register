﻿using Autofac;
using Autofac.Builder;
using AutoRegistDependency.Component;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Implement;
using AutoRegistDependency.Utils;
using AutoRegister;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.RegistrationHandler
{
    /// <summary>
    /// 非泛型聚合服务处理
    /// Non generic aggregation service processing
    /// </summary>
    internal class AggregateServiceHandler : AbstractRegistrationHandler
    {
        public override bool CanRegister(ComponentDefinition component)
        {
            return component.ComponentType == ComponentType.AggregateService && !component.InjectType.IsGenericType;
        }

        protected override IRegistrationBuilder<object, object, object> Register(ContainerBuilder containerBuilder, ComponentDefinition component)
        {
            var register = containerBuilder.Register(c =>
            {
                return AutoFacHelper.CreateProxyInstance(new AggregateServiceInterceptor(c.Resolve<IComponentContext>(), component.InjectType, component), component.InjectType);
            });
            return register;
        }

        public override IRegistrationBuilder<object, object, object> StartRegistration(ContainerBuilder containerBuilder, ComponentDefinition component, bool isInterface, Type[] types, bool hasPostProcessor, HostBuilderContext hostBuilderContext)
        {
            if(CanRegister(component))
            {
                var registrationBuilder = Register(containerBuilder,component);
                RegistrationAs(registrationBuilder,component,types);
                registrationBuilder.OnlyIf(componentRegistry => ShouldRegistration(component,hostBuilderContext, componentRegistry));
                return registrationBuilder;
            }
            else
            {
                return NextHandler?.StartRegistration(containerBuilder, component, isInterface, types, hasPostProcessor, hostBuilderContext);
            }
            
        }
    }
}
