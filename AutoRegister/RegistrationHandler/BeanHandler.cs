﻿using Autofac;
using Autofac.Builder;
using AutoRegistDependency.Component;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoRegistDependency.RegistrationHandler
{
    /// <summary>
    /// 非泛型bean处理
    /// Non generic bean processing
    /// </summary>
    internal class BeanHandler : AbstractRegistrationHandler
    {
        public override bool CanRegister(ComponentDefinition component)
        {
            if(component.ComponentType == ComponentType.Bean)
            {
                BeanDefinition beanComponent =(BeanDefinition)component;
                return beanComponent.CanRegist && !beanComponent.IsGeneric;
            }
            else
            {
                return false;
            }
        }

        protected override IRegistrationBuilder<object, object, object> Register(ContainerBuilder containerBuilder, ComponentDefinition component)
        {
            BeanDefinition beanComponent = (BeanDefinition)component;
            var r = containerBuilder.Register((context,paramters) =>
            {
                var configurationInstance = context.Resolve(beanComponent.LocationType);
                var beanInstance = AutoFacHelper.InvokeMethod(configurationInstance, beanComponent.BeanCreatedMethod, context, paramters,beanComponent.IsAsync);
                return beanInstance;
            });
            return r;
        }
        protected override void RegistrationAs(IRegistrationBuilder<object, object, object> registrationBuilder, ComponentDefinition component, Type[] types)
        {
            BeanDefinition beanComponent = (BeanDefinition)component;
            base.RegistrationAs(registrationBuilder,component,new Type[1] { beanComponent.InjectType });
        }
    }
}
