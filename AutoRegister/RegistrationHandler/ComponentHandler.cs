﻿using Autofac;
using Autofac.Builder;
using Autofac.Features.AttributeFilters;
using AutoRegistDependency.Component;
using AutoRegistDependency.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoRegistDependency.RegistrationHandler
{
    /// <summary>
    /// 非泛型class组件注册处理类
    /// Non generic class component registration processing class
    /// </summary>
    internal class ComponentHandler : AbstractRegistrationHandler
    {
        public override bool CanRegister(ComponentDefinition component)
        {
            return component.ComponentType != ComponentType.Bean && component.ComponentType != ComponentType.AggregateService && !component.InjectType.IsGenericType;
        }

        protected override IRegistrationBuilder<object, object, object> Register(ContainerBuilder containerBuilder, ComponentDefinition component)
        {
            var r = containerBuilder
                .RegisterType(component.InjectType)
                .WithAttributeFiltering();
            if (component.ConstructorTypes.Count() > 0)
            {
                r.UsingConstructor(component.ConstructorTypes);
            }
            return r;
        }
    }
}
