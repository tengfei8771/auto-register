﻿using Autofac;
using Autofac.Builder;
using Autofac.Features.AttributeFilters;
using AutoRegistDependency.Component;
using AutoRegistDependency.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoRegistDependency.RegistrationHandler
{
    /// <summary>
    /// 泛型class组件处理
    /// Processing of generic class components
    /// </summary>
    internal class GenericComponentHandler : AbstractRegistrationHandler
    {
        public override bool CanRegister(ComponentDefinition component)
        {
            return component.ComponentType != ComponentType.Bean && component.ComponentType != ComponentType.AggregateService && component.InjectType.IsGenericType;
        }

        protected override IRegistrationBuilder<object, object, object> Register(ContainerBuilder containerBuilder, ComponentDefinition component)
        {
            var r = containerBuilder
                .RegisterGeneric(component.InjectType)
                .WithAttributeFiltering();
            if (component.ConstructorTypes.Count() > 0)
            {
                r.UsingConstructor(component.ConstructorTypes);
            }
            return r;
        }
    }
}
