﻿using AutoRegistDependency.Attributes;
using fastNpoi.Impl;
using fastNpoi.Interface;
using WebTest.Condition;
using WebTest.ConfigModel;
using WebTest.Services.Test;

namespace WebTest.Bean
{
	[Component]
	public class ConfigurationTest
	{
		//[Bean]
		//public LogModel GetBeanObject([ComponentFilter(name: "Test")] ITest test)
		//{
		//	test.Say();
		//	return new LogModel()
		//	{
		//		Default = "我测你妈"
		//	};
		//}
		[Bean]
		[DependsOn(typeof(Test))]
		//[Conditional(typeof(ConditionClass))]
		public async Task<BeanTest> TestBean(ITest test)
		{
			string str=test.Say();
            BeanTest beanTest = null;
			await Task.Run(() =>
			{
				beanTest = new BeanTest();
			});
			return beanTest;
		}
		[Bean]
		public BeanTest1<T> TestBean1<T>()
		{
			Console.WriteLine("成功注册！");
			return new BeanTest1<T>();
		}
		[Bean]
		public IFastNpoi FastNpoi()
		{
			return new FastNpoi();
		}
	}
}
