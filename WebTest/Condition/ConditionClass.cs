﻿using Autofac.Core.Registration;
using AutoRegistDependency.Component;
using AutoRegistDependency.Interface;
using System.Reflection;

namespace WebTest.Condition
{
	public class ConditionClass : ICondition
	{
		public ConditionClass(int i=5) { }
		public bool Matches(HostBuilderContext hostBuilderContext, IComponentRegistryBuilder componentRegistry, ComponentDefinition component)
		{
			if (hostBuilderContext.HostingEnvironment.IsDevelopment())
			{
				return true;
			}
			else
			{
				return true;
			}
		}
	}
}
