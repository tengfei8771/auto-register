﻿using AutoRegistDependency.Attributes;

namespace WebTest.ConfigModel
{
	[ConfigurationProperties(path: "Logging:LogLevel")]
	public class LogModel
	{
		//[Value("Nacos:Namespace")]
		public string Default { get; set; }

		public string TestInterceptor()
		{
			return Default;
		}
	}
}
