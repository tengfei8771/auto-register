using AutoRegistDependency;
using AutoRegistDependency.Attributes;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Interface;
using fastNpoi.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Net.NetworkInformation;
using WebTest.Bean;
using WebTest.ConfigModel;
using WebTest.Services;
using WebTest.Services.GenericTest;
using WebTest.Services.Test;
using SqlSugar;

namespace WebTest.Controllers
{
	[ApiController]
	[Component]
	[Route("[controller]")]
	//[DependsOn(typeof(Test1))]
	public class WeatherForecastController : ControllerBase
	{
		private readonly LogModel logModel;
		private static readonly string[] Summaries = new[]
		{
		"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
		};

		private readonly ILogger<WeatherForecastController> _logger;
		[Autowired]
		private IFastNpoi fastNpoi;
		[Autowired(name:"Test1")]
        private ITest test1;
		private Test test;
		//[Value("Logging:LogLevel")]
		//private LogModel testValue;
		[Autowired]
		private BeanTest1<LogModel> beanTest1;
		[Autowired(name: "456")]
		private IGenercTest<LogModel> genercTest;
		//[Autowired]
		//private IEventBus eventPublisher;
		[Autowired]
		private IBeanTest ibeanTest;
        [Autowired]
        private BeanTest beanTest;
		[Autowired]
		private IMyAggregateService myAggregateService;
		//[Autowired]
		//private void GetTest([ComponentFilter(name: "Test")] ITest test)
		//{
		//	//this.test = test;
		//}
		//public WeatherForecastController(LogModel logModel,ILogger<WeatherForecastController> logger, ITest test)
		//{
		//	_logger = logger;
		//	this.test1= test;
		//	this.logModel = logModel;
		//	//this.genercTest = genercTest;
		//}

		[HttpGet(Name = "GetWeatherForecast")]
		public IEnumerable<WeatherForecast> Get()
		{
            //List<int> list=eventPublisher.PublishAsync<LogModel, int>(testValue).GetAwaiter().GetResult();
            var test = test1.Say();
			Console.WriteLine(myAggregateService.test1().Say());
			Console.WriteLine(myAggregateService.test.Say());
			Console.WriteLine($"test:{test1.Say()}");
			//int prop = test.MyProperty;
			Console.WriteLine($"genercTest:{genercTest.Say()}");
			Console.WriteLine($"{beanTest.Test()}");
			return Enumerable.Range(1, 5).Select(index => new WeatherForecast
			{
				Date = DateTime.Now.AddDays(index),
				TemperatureC = Random.Shared.Next(-20, 55),
				Summary = Summaries[Random.Shared.Next(Summaries.Length)]
			})
			.ToArray();
		}
	}
}