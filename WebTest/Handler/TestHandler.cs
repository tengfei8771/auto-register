﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Interface;

namespace WebTest.Handler
{
    [Component]
    public class TestHandler : IValueHandler
    {
        public object Execute(object value)
        {
            Console.WriteLine(value);
            return value;
        }
    }
}
