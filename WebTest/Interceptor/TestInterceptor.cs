﻿using AutoRegistDependency.Abstract;
using AutoRegistDependency.Attributes;
using Castle.DynamicProxy;
using System.Reflection;
using WebTest.Bean;
using WebTest.Services.Test;

namespace WebTest.Interceptor
{
	//[Component]
	public class TestInterceptor : AbstractInterceptor
	{
		public TestInterceptor(IEnumerable<Assembly> dlls) : base(dlls)
		{
		}

		public override IEnumerable<Type> GetInterceptorTypes()
		{
			return new Type[] { typeof(Test),typeof(Test1),typeof(BeanTest) };
		}

		public override void InterceptAsynchronous(IInvocation invocation)
		{
			Console.WriteLine($"before:{nameof(InterceptAsynchronous)}");
			invocation.Proceed();
			Console.WriteLine($"after:{nameof(InterceptAsynchronous)}");
		}

		public override void InterceptAsynchronous<TResult>(IInvocation invocation)
		{
			Console.WriteLine($"before:{nameof(InterceptAsynchronous)}");
			invocation.Proceed();
			Console.WriteLine($"after:{nameof(InterceptAsynchronous)}");
		}

		public override void InterceptSynchronous(IInvocation invocation)
		{
			Console.WriteLine($"before:{nameof(InterceptSynchronous)}");
			invocation.Proceed();
			Console.WriteLine($"after:{nameof(InterceptSynchronous)}");
		}
	}
}
