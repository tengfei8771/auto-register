﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Attributes.Condition;
using Quartz;
using WebTest.Condition;
using WebTest.ConfigModel;
using WebTest.Handler;
using WebTest.Services.Test;

namespace WebTest.Job
{
    [Job(cron: "55 * * * * ? ")]
    [Conditional(typeof(ConditionClass))]
    [ConditionalOnProperty("Logging:LogLevel:Default", "Information")]
    //[ConditionalOnMissBean("Test",typeof(Test))]
    public class TestJob : IJob
    {
        [Value(path: "Logging:LogLevel", valueHandler: typeof(TestHandler))]
        private LogModel testValue;
        //public TestJob(LogModel testValue)
        //{
        //	this.testValue = testValue;
        //}

        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                Console.WriteLine(testValue.Default);
            });
        }
    }
}
