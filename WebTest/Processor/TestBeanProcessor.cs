﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Interface;

namespace WebTest.Processor
{
	//[Component]
	public class TestBeanProcessor : IBeanPostProcessor
	{
		public object PostProcessAfterInitialization(object bean)
		{
			Console.WriteLine($"PostProcessAfterInitialization:{bean.GetType()}");
			return bean;
		}

		public object PostProcessBeforeInitialization(object bean)
		{
			Console.WriteLine($"PostProcessBeforeInitialization:{bean.GetType()}");
			return bean;
		}
	}
}
