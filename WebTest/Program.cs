using Autofac.Extensions.DependencyInjection;
using Autofac;
using AutoRegister;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection.Extensions;
using AutoRegistDependency.Extensions;
using WebTest.Services;
using Autofac.Core.Resolving.Pipeline;
using WebTest.Services.GenericTest;

WebApplicationBuilder? builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// 注册服务到Nacos
//builder.Services.AddNacosAspNet(builder.Configuration, section: "nacos");
// 添加配置中心
//builder.Configuration.AddNacosV2Configuration(builder.Configuration.GetSection("Nacos"));
Console.WriteLine(builder.Configuration.GetSection("Logging:LogLevel:Default").Value);
builder.Host.ConfigureContainer((HostBuilderContext hostBuilderContext, ContainerBuilder containerBuilder) =>
{
	containerBuilder.RegisterModule(new Register(hostBuilderContext));
	//new Register(hostBuilderContext).RegisterType(containerBuilder);
});
builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Services.AddControllers().AddControllersAsServices();
builder.Services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
WebApplication? app = builder.Build();
app.Services.QuartzRegist();
var config = app.Services.GetService<IConfiguration>();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
