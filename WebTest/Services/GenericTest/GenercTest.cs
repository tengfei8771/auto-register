﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Enum;

namespace WebTest.Services.GenericTest
{
	[Component(name: "123")]
	[RegistAs(typeof(IGenercTest<>))]
	public class GenercTest<T> : IGenercTest<T> where T : class
	{
		public string Say()
		{
			return nameof(T);
		}
	}
	public record RecordClass
	{

	}
}
