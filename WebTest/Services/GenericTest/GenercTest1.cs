﻿using AutoRegistDependency.Attributes;
using WebTest.Services.Test;

namespace WebTest.Services.GenericTest
{
	[Component(name: "456")]
	public class GenercTest1<T> : IGenercTest<T>,ITest
	{
		[Autowired]
		private WebTest.Services.Test.Test test;

        public string Run()
        {
            throw new NotImplementedException();
        }

        public string Say()
		{
			test.Say();
			return "456";
		}

	}
}
