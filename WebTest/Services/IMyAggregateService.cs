﻿using AutoRegistDependency.Attributes;
using WebTest.Services.Test;

namespace WebTest.Services
{
	[AggregateService]
	public interface IMyAggregateService
	{
		[Autowired(name: "Test1")]
		ITest test1();
		[Autowired(name: "Test")]
		ITest test { get; }
	}
}
