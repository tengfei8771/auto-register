﻿namespace WebTest.Services.Test
{
	public interface ITest
	{
		string Say();
		string Run();
	}
}
