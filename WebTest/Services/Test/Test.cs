﻿using AutoRegistDependency.Attributes;
using WebTest.Condition;

namespace WebTest.Services.Test
{
	[Component(name: "Test")]
	//[Conditional(typeof(ConditionClass))]
	public class Test : ITest
	{
		//[Autowired]
		//private Test1 test1;
        public int MyProperty { get; set; }
        public Test() { }
		public string Say()
		{
			return nameof(Test);
		}

        public string Run()
        {
            throw new NotImplementedException();
        }
    }
}
