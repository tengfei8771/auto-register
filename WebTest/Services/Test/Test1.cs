﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Attributes.Processor;
using WebTest.Bean;

namespace WebTest.Services.Test
{
	[Component]
	public class Test1 : ITest
	{
		[Autowired]
		private Test test;
		
		public Test1(int i=1)
		{
			Console.WriteLine("4165343521");
		}
		[Constructor]
		public Test1(ConfigurationTest configurationTest)
		{
			Console.WriteLine("4165343521");
		}
		public virtual string Say()
		{
            //test.Say();
            Run();
            return nameof(Test1);
		}
		[OnActivating]
		public Task OnActivating()
		{
			return Task.Run(() =>
			{
				Console.WriteLine("OnActivating");
			});
            
        }
		[OnActivated]
        public Task OnActivated()
        {
            return Task.Run(() =>
            {
                Console.WriteLine("OnActivated");
            });
        }
		[OnRelease]
        public void OnRelease()
        {
            Console.WriteLine("OnRelease");
        }

        public string Run()
        {
			return "Run";
        }
    }
}
