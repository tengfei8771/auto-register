﻿using AutoRegistDependency.Attributes;
using AutoRegistDependency.Enum;
using AutoRegistDependency.Interface;
using Newtonsoft.Json;
using WebTest.ConfigModel;

namespace WebTest.Services
{
	[Component]
	public class WorkListener: IEventAsyncHandler<LogModel, int>
	{
		public async Task<int> Handle(LogModel eventData)
		{
			await Task.Run(() =>
			{
				Console.WriteLine("123");
				Task.Delay(500);
			}) ;
			return 0;
		}
	}
}
